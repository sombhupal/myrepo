package com.findit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.findit.controller.UserController;
import com.findit.request.UserRequest;
import com.findit.response.UserResponse;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FinditApplicationTests {

	
	@Autowired
	private UserController userController;
	
	@Test
	public void contextLoads() {
		UserRequest userRequest = new UserRequest();
		userRequest.setEmail("som@gmail.com");
		userRequest.setFirstName("som");
		userRequest.setLastName("bhupal");
		userRequest.setPassword("bhupal123");
		
		userController.registration(userRequest, null);
		UserResponse body = this.userController.registration(userRequest, null);
		System.out.println(body);
	}

}
