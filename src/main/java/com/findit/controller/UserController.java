package com.findit.controller;


import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.findit.request.UserRequest;
import com.findit.response.UserResponse;
import com.findit.service.UserService;

@RestController
@RequestMapping(value="user")
public class UserController {
	
	@Autowired
	UserService userService;
	
	@RequestMapping(value="reg" ,method=RequestMethod.POST ,produces={MediaType.APPLICATION_JSON_VALUE})
	public UserResponse registration(@Valid @ModelAttribute UserRequest userRequest, BindingResult result){
		UserResponse userResponse = new UserResponse();
		if(result.hasErrors()) {
			List<String> list = new ArrayList<String>();
			
			List<ObjectError> allErrors = result.getAllErrors();
			for(ObjectError oe :allErrors){
				list.add(oe.getDefaultMessage());
			}
			userResponse.setErrors(list);
			return userResponse;
		}
		return userService.registration(userRequest);
	}
	
}
