package com.findit.util;

import com.findit.entity.User;
import com.findit.request.UserRequest;

public class UserUtil {
	
	public static User loadUser(UserRequest userRequest){
		User user=new User();
		user.setFirstName(userRequest.getFirstName());
		user.setLastName(userRequest.getLastName());
		user.setEmail(userRequest.getEmail());
		user.setPassword(userRequest.getPassword());
		
		return user;
		
	}

}
