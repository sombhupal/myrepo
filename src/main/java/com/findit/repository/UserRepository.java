package com.findit.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.findit.entity.User;

public interface UserRepository extends MongoRepository<User, String>{

}
