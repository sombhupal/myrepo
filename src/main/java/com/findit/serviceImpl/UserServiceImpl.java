package com.findit.serviceImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.findit.entity.User;
import com.findit.repository.UserRepository;
import com.findit.request.UserRequest;
import com.findit.response.UserResponse;
import com.findit.service.UserService;
import com.findit.util.UserUtil;

@Service(value="userServiceImpl")
public class UserServiceImpl implements UserService{

	private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
	
	@Autowired
	UserRepository userRepository;
	
	
	@Override
	public UserResponse registration(UserRequest userRequest) {
		logger.info("userRequest :: " + userRequest);
		UserResponse userResponse=new UserResponse();
		User user = UserUtil.loadUser(userRequest);
		 user = userRepository.save(user);
		 userResponse.setUser(user);
		return userResponse;
	}

}
