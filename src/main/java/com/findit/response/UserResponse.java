package com.findit.response;

import com.findit.entity.User;

public class UserResponse extends Abstractresponse{

	private User user;

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}
	
	
}
