package com.findit.service;

import com.findit.request.UserRequest;
import com.findit.response.UserResponse;

public interface UserService {

	UserResponse registration(UserRequest userRequest);
}
